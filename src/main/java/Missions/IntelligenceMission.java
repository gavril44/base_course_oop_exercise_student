package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.CameraUser;
import AerialVehicles.SensorsUsers;
import Entities.Coordinates;
import Pilot.Pilot;

public class IntelligenceMission extends Mission {

    SensorsUsers my_sensor;

    public IntelligenceMission(Coordinates destination, Pilot my_pilot, AerialVehicle aerialVehicle,
                               SensorsUsers sensorsUsers) {
        super(destination, my_pilot, aerialVehicle);
        this.my_sensor = sensorsUsers;
    }

    @Override
    public String execute_mission() {
        return getPilot_name() + ": " + get_vehicle_name() + "Collecting Data in : " + getDestination() + " with:"
                + my_sensor.get_sensor_name();
    }


}
