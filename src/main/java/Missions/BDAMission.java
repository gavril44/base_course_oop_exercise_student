package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.CameraUser;
import AerialVehicles.WeaponUsers;
import Entities.Coordinates;
import Pilot.Pilot;

public class BDAMission extends Mission {

    CameraUser my_camera_user;

    public BDAMission(Coordinates destination, Pilot my_pilot, AerialVehicle aerialVehicle,
                      CameraUser cameraUser) {
        super(destination, my_pilot, aerialVehicle);
        this.my_camera_user = cameraUser;
    }

    @Override
    public String execute_mission() {
        return getPilot_name() + ": " + get_vehicle_name() + "taking pictures of : " + getDestination() + " with:"
                + my_camera_user.get_camera_name();
    }


}
