package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.FighterPlane;
import AerialVehicles.WeaponUsers;
import Entities.Coordinates;
import Pilot.Pilot;

public class AttackMission extends Mission {

    WeaponUsers my_weapon_user;

    public AttackMission(Coordinates destination, Pilot my_pilot, AerialVehicle aerialVehicle, WeaponUsers weaponUsers) {
        super(destination, my_pilot, aerialVehicle);
        this.my_weapon_user = weaponUsers;
    }

    @Override
    public String execute_mission() {
        return getPilot_name() + ": " + get_vehicle_name() + "Attacking " + getDestination() + " with: "
                + my_weapon_user.get_weapon_name() + "X" + my_weapon_user.get_ammu_left();
    }


}
