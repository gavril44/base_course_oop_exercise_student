package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Pilot.Pilot;

public abstract class Mission{

    private Coordinates destination;
    private Pilot my_pilot;
    private AerialVehicle my_vehicle;

    public Mission(Coordinates destination, Pilot my_pilot, AerialVehicle aerialVehicle) {
        this.destination = destination;
        this.my_pilot = my_pilot;
        this.my_vehicle = aerialVehicle;
    }

    public void begin() {
        System.out.println("Beginnig Mission!");
        my_vehicle.fly_to(destination);
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        my_vehicle.land(my_vehicle.getMy_home_coordinates());
    }

    public void finish() {
        execute_mission();
        my_vehicle.land(my_vehicle.getMy_home_coordinates());
        System.out.println("Finish Mission!");
    }

    public String getPilot_name() {
        return my_pilot.getPilot_name();
    }

    public String get_vehicle_name() {
       return my_vehicle.get_vehicle_name();
    }

    public Coordinates getDestination() {
        return destination;
    }


    abstract public String execute_mission();

}
