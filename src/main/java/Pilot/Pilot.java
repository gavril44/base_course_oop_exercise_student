package Pilot;

public abstract class Pilot {
    String pilot_name;

    public Pilot(String pilot_name) {
        this.pilot_name = pilot_name;
    }

    public void setPilot_name(String pilot_name) {
        this.pilot_name = pilot_name;
    }

    public String getPilot_name() {
        return pilot_name;
    }
}
