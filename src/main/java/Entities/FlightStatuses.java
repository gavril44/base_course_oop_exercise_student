package Entities;

public enum FlightStatuses {
    READY, NOT_READY, IN_AIR;
}
