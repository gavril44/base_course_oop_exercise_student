package Entities.WeponSystems;

public interface WeaponSystems {
    public int get_ammunition_left();
    public void fire();
    public String get_ammu_name();
}
