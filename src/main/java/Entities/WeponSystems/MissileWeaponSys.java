package Entities.WeponSystems;

import Entities.Missiles.FirebleMissiles;
import Entities.WeponSystems.WeaponSystems;

import java.util.ArrayList;

public class MissileWeaponSys implements WeaponSystems {
    // takes a list of missiles and can fire them.

    ArrayList<FirebleMissiles> missiles_list;
    int ammunition;

    public MissileWeaponSys(ArrayList<FirebleMissiles> missiles_list) {
        this.missiles_list = missiles_list;
        this.ammunition = missiles_list.size();
    }

    private void add_missile(FirebleMissiles missile) {
        missiles_list.add(missile);
    }

    private void set_missiles(ArrayList<FirebleMissiles> missiles_list1) {
        missiles_list = missiles_list1;
    }


    private void count_ammunition() {
        ammunition = missiles_list.size();
    }

    @Override
    public void fire() {
        // how to fire with missiles
    }

    @Override
    public int get_ammunition_left() {
        count_ammunition();
        return ammunition;
    }

    @Override
    public String get_ammu_name() {
        //checks what missile is in
        return missiles_list.toString();
    }
}
