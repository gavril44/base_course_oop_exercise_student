package Entities.BDASystems;

import Entities.Cameras.Cameras;

public class CameraBDASystem implements BDASystems{

    Cameras my_camera;

    public CameraBDASystem(Cameras camera) {
        this.my_camera = camera;
    }

    @Override
    public void take_BDA() {
        //implementation
        my_camera.take_picture();
    }
}
