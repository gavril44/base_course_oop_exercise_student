package Entities.IntelligenceSystems;

import Entities.Sensors.Sensor;

public class SensorIntelSystem implements IntelligenceSystems {
    Sensor my_sensor;

    public SensorIntelSystem(Sensor sensor) {
        this.my_sensor = sensor;
    }

    @Override
    public void take_Intelligence() {

        // use the sensor to take a measurement aka "intelligence"
        my_sensor.take_measurement();
    }
}
