package AerialVehicles;


import Entities.Coordinates;
import Entities.FlightStatuses;

public abstract class AerialVehicle {

    private int REPAIR_CYCLE_SIZE;
    private int flight_hours_since_last_repair;
    private FlightStatuses flight_status;
    private Coordinates my_home_coordinates;

    public AerialVehicle(int flight_hours_since_last_repair, FlightStatuses start_flight_status, Coordinates my_home_coordinates) {
        this.flight_hours_since_last_repair = flight_hours_since_last_repair;
        this.flight_status = start_flight_status;
        this.my_home_coordinates = my_home_coordinates;
    }

    public int get_REPAIR_CYCLE_SIZE() {
        return REPAIR_CYCLE_SIZE; // od course this is still not defined
    }

    public Coordinates getMy_home_coordinates() {
        return my_home_coordinates;
    }

    public void fly_to(Coordinates destination) {
        if (flight_status.equals(FlightStatuses.READY) || flight_status.equals(FlightStatuses.IN_AIR)) {
            System.out.println("Flying to : " + destination.toString());
            flight_status = FlightStatuses.IN_AIR;
        }
        else {
            System.out.println("Ariel Vehicle isnt ready to fly");
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on : " + destination.toString());
        check(flight_hours_since_last_repair, get_REPAIR_CYCLE_SIZE());
    }

    public void check(int flight_hours_since_last_repair, int REPAIR_CYCLE_SIZE) {
        if (flight_hours_since_last_repair >= REPAIR_CYCLE_SIZE) {
            set_Flight_status(FlightStatuses.NOT_READY);
            repair();
        }
        else {
            set_Flight_status(FlightStatuses.READY);
        }
    }

    public void repair() {
        flight_hours_since_last_repair = 0;
        set_Flight_status(FlightStatuses.READY);
    }

    public void set_Flight_status(FlightStatuses new_status) {
        flight_status = new_status;
    }

    abstract public String get_vehicle_name();

}
