package AerialVehicles;

import Entities.BDASystems.BDASystems;
import Entities.Coordinates;
import Entities.FlightStatuses;
import Entities.IntelligenceSystems.IntelligenceSystems;
import Entities.WeponSystems.WeaponSystems;

public class Kochav extends HermesDrones{

    WeaponSystems my_weapon_system;

    public Kochav(int flight_hours_since_last_repair, FlightStatuses flight_status,
                 Coordinates my_home_coordinates, IntelligenceSystems intelligenceSystems, WeaponSystems weaponSystems,
                   BDASystems bdaSystem) {
        super(flight_hours_since_last_repair, flight_status, my_home_coordinates, intelligenceSystems, bdaSystem);
        this.my_weapon_system = weaponSystems;
    }

    @Override
    public String get_vehicle_name() {
        return "Kochav";
    }
}
