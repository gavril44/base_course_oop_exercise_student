package AerialVehicles;

import Entities.Coordinates;
import Entities.FlightStatuses;
import Entities.IntelligenceSystems.IntelligenceSystems;

public abstract class HaronDrones extends DronePlane {

    int REPAIR_CYCLE_SIZE_OF_HARON = 150;

    public HaronDrones(int flight_hours_since_last_repair, FlightStatuses flight_status, Coordinates my_home_coordinates,
                       IntelligenceSystems intelligenceSystems) {
        super(flight_hours_since_last_repair, flight_status, my_home_coordinates, intelligenceSystems);
    }

    @Override
    public int get_REPAIR_CYCLE_SIZE() {
        return REPAIR_CYCLE_SIZE_OF_HARON;
    }
}
