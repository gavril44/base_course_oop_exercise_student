package AerialVehicles;

import Entities.BDASystems.BDASystems;
import Entities.Coordinates;
import Entities.FlightStatuses;
import Entities.IntelligenceSystems.IntelligenceSystems;

public abstract class HermesDrones extends DronePlane {

    final int REPAIR_CYCLE_SIZE_OF_HERMES = 100;
    private BDASystems my_bda_system;

    public HermesDrones(int flight_hours_since_last_repair, FlightStatuses flight_status, Coordinates my_home_coordinates,
                        IntelligenceSystems intelligenceSystems, BDASystems bdaSystem) {
        super(flight_hours_since_last_repair, flight_status, my_home_coordinates, intelligenceSystems);
        this.my_bda_system = bdaSystem;
    }

    @Override
    public int get_REPAIR_CYCLE_SIZE() {
        return REPAIR_CYCLE_SIZE_OF_HERMES;
    }
}
