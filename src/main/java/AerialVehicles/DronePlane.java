package AerialVehicles;

import Entities.Coordinates;
import Entities.FlightStatuses;
import Entities.IntelligenceSystems.IntelligenceSystems;

public abstract class DronePlane extends AerialVehicle{

    IntelligenceSystems my_intelligence_system;

    public DronePlane(int flight_hours_since_last_repair, FlightStatuses flight_status, Coordinates my_home_coordinates
    , IntelligenceSystems intelligenceSystems) {
        super(flight_hours_since_last_repair, flight_status, my_home_coordinates);
        this.my_intelligence_system = intelligenceSystems;
    }

    private String hover_over_location(Coordinates destination) {
        set_Flight_status(FlightStatuses.IN_AIR);
        String text = "Hovering over : " + destination.toString();
        System.out.println(text);
        return text;
        //implementation
    }

    private void use_intelligence_system() {
        my_intelligence_system.take_Intelligence();
    }



}
