package AerialVehicles;


import Entities.Coordinates;
import Entities.FlightStatuses;
import Entities.IntelligenceSystems.IntelligenceSystems;
import Entities.WeponSystems.WeaponSystems;

public class F16 extends FighterPlane{

    IntelligenceSystems my_intelligence_system;

    public F16(int flight_hours_since_last_repair, FlightStatuses flight_status, Coordinates my_home_coordinates,
               WeaponSystems weaponSystems, IntelligenceSystems intelligenceSystems) {
        super(flight_hours_since_last_repair, flight_status, my_home_coordinates, weaponSystems);
        this.my_intelligence_system = intelligenceSystems;
    }

    @Override
    public String get_vehicle_name() {
        return "F16";
    }
}
