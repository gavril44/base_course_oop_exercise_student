package AerialVehicles;

import Entities.Coordinates;
import Entities.FlightStatuses;
import Entities.WeponSystems.WeaponSystems;

public abstract class FighterPlane extends AerialVehicle implements WeaponUsers {

    int REPAIR_CYCLE_SIZE_OF_FIGHTER = 250;
    WeaponSystems my_weapon_system;

    public FighterPlane(int flight_hours_since_last_repair, FlightStatuses flight_status, Coordinates my_home_coordinates
    , WeaponSystems weaponSystems) {
        super(flight_hours_since_last_repair, flight_status, my_home_coordinates);
        this.my_weapon_system = weaponSystems;
    }

    @Override
    public int get_REPAIR_CYCLE_SIZE() {
        return REPAIR_CYCLE_SIZE_OF_FIGHTER;
    }

    @Override
    public String get_weapon_name() {
        return my_weapon_system.get_ammu_name();
    }

    @Override
    public String get_ammu_left() {
        return Integer.toString(my_weapon_system.get_ammunition_left());
    }
}


