package AerialVehicles;

import Entities.Coordinates;
import Entities.FlightStatuses;
import Entities.IntelligenceSystems.IntelligenceSystems;
import Entities.WeponSystems.WeaponSystems;

public class Eitan extends HaronDrones{
    WeaponSystems my_weapon_system;

    public Eitan(int flight_hours_since_last_repair, FlightStatuses flight_status,
                 Coordinates my_home_coordinates, IntelligenceSystems intelligenceSystems, WeaponSystems weaponSystems) {
        super(flight_hours_since_last_repair, flight_status, my_home_coordinates, intelligenceSystems);
        this.my_weapon_system = weaponSystems;
    }


    @Override
    public String get_vehicle_name() {
        return "Eitan";
    }

    //what makes this one special
}
